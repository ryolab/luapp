
#include <luapp/luapp.hpp>
#include <stdlib.h>

extern "C"
{
#include <lualib.h>
}

namespace lua
{
    static error_function s_errHandler = nullptr;

    void set_error_handler(error_function && cb)
    {
        s_errHandler = std::move(cb);
    }

    static void raise_error(int errCode, std::string const & msg)
    {
        if (s_errHandler)
            s_errHandler(errCode, msg);
        else
        {
            const char* sErrCode;
            switch (errCode)
            {
            case LUA_ERRRUN:
                sErrCode = "LUA_ERRUN";
                break;
            case LUA_ERRSYNTAX:
                sErrCode = "LUA_ERRSYNTAX";
                break;
            case LUA_ERRMEM:
                sErrCode = "LUA_ERRMEM";
                break;
            case LUA_ERRERR:
                sErrCode = "LUA_ERRERR";
                break;
            default:
                sErrCode = "UNKNOWN";
                break;
            }
            fprintf(stderr, "[Lua] %s: %s", sErrCode, msg.c_str());
        }
    }

    bool call(lua::handle L, int narg, int nresult)
    {
        int res = lua_pcall(L, narg, nresult, 0);
        switch (res)
        {
        case 0:
            // successful
            return true;
        default:
            raise_error(res, lua_tostring(L, -1));
            lua_pop(L, 1);
            return false;
        }
    }

    namespace detail
    {
        static void * allocator(void *, void *ptr, size_t osize, size_t nsize)
        {
            (void)osize;
            if (0 == nsize) 
            {
                free(ptr);
                return nullptr;
            }
            else
                return realloc(ptr, nsize);
        }
    }

    state::state(initial what)
    {
        L = lua_newstate(detail::allocator, nullptr);

        switch (what)
        {
        case initial::default_libs:
            luaL_openlibs(L);
            break;
        default:
            break;
        }
    }

    state::~state()
    {
        if (nullptr != L)
            lua_close(L);
    }

    inline state::state(state && other)
    {
        L = other.L;
        other.L = nullptr;
    }

    state & state::operator =(state && other)
    {
        L = other.L;
        other.L = nullptr;
        return *this;
    }

    ref_base state::get_global(std::string const & name) const
    {
        lua_getglobal(L, name.c_str());
        return ref_base(L);
    }

    ref_base state::call(std::string const & text) const
    {
        top_guard grd(load(text));
        if (grd)
        {
            if (lua::call(L, 0, LUA_MULTRET))
            {
                int res = lua_gettop(L);
                if (res > grd.top)
                    return ref_base(L);
                else
                    return ref_base();
            }
        }
        return ref_base();
    }

    lua::ref_base state::load(std::string const & text) const
    {
        int res = luaL_loadstring(L, text.c_str());
        switch (res)
        {
        case 0:
            // successful
            break;

        default:
            raise_error(res, lua_tostring(L, -1));
            return ref_base();
        }

        return ref_base(L);
    }

    lua::ref_base state::load(const char * buff, size_t sz, std::string const & name) const
    {
        int res = luaL_loadbuffer(L, buff, sz, name.c_str());
        switch (res)
        {
        case 0:
            // successful
            break;

        default:
            raise_error(res, lua_tostring(L, -1));
            return ref_base();
        }

        return ref_base(L);
    }

    static state * s_state = nullptr;

    state * state::getInstance()
    {
        if (nullptr == s_state)
        {
            s_state = new(std::nothrow) state(initial::default_libs);
        }
        return s_state;
    }

    void state::destroyInstance()
    {
        if (nullptr != s_state)
        {
            delete s_state;
            s_state = nullptr;
        }
    }
}
