LUAPP
===================

提供一個選項來輕鬆介接C++與LUA之間的橋樑。

----------

Documents
-------------

## 編譯

目前僅在Visual Studio 2015上編譯並測試過，未來將確保Android、iOS及Linux能順利運作。

### Visual Studio 2015

>打開luapp.sln並編譯即可。

## 使用方法簡介

### :cyclone:Include

將Include目錄設置於專案內資料夾"include"

### :cyclone:簡易範例

**main.cpp**

    #include <luapp/luapp.h>
    #include <stdio.h>
    
    class foo
    {
    public:
        ~foo()
        {
            printf("foo down!\n");
        }
    
        int hello(const char* say, std::string const & what)
        {
            printf("%s %s\n", say, what.c_str());
            return value;
        }
    
        int value = 567;
    };
    
    LUAPP_LIBRARY(foo, lua::undefined_t, "foo",
        LUAPP_TABLE(
            LUAPP_NEW,
            LUAPP_FUNCTION(hello),
            LUAPP_SYNTHESIZE(value)
        );
    );
    
    int main()
    {
        lua::open_lib<foo>();
        auto S = lua::state::getInstance();
        auto L = S->native_handle();
        luaL_dofile(L, "scripts/foo.lua");
        lua::ref<std::string> fn = S->get_global("hey");
        printf("%s\n", fn("Hi Lua!").get().c_str());
        return 0;
    }

**foo.lua**

    local o = foo.new ()
    print ("before hello:" .. o:value ())
    o:set_value(123)
    local n = o:hello('Hello', 'Cpp!')
    print("after hello:" .. n)
    
    function hey (s)
        print(s)
        return 'Finished!'
    end

**執行結果**

    before hello:567
    Hello Cpp!
    after hello:123
    Hi Lua!
    Finished!
    foo down!
    
### :cyclone:說明

**支援型態（箭頭代表可轉換方向）：**

>function pointer → lua function

>member function pointer → lua function

>std::string ←→ lua string

>const char * ←→ lua string

>signed/unsigned integer types ( char, short, int, long, long long ) ←→ lua integer

>floating types ( float, double ) ←→ lua number

>任何被定義過（使用LUAPP_LIBRARY）的型態 ←→ lua userdata （包含metatable）

>任何被定義過的型態指標 ←→ lua light userdata （含metatable）

>member pointer（支援以上型態） ←→ lua function 

>`std::function<signature>` ←→ lua function

>`lua::ref<Type>` ←→ reference to lua value

**注意：**

>member pointer 將會以 `o:member()` 取值及 `o:set_member(value)`賦值

>lua內使用LUAPP_LIBRARY定義過的型態指標來代表參考

>lua內使用LUAPP_LIBRARY定義過的型態無論任何修飾詞（const, volitile, &, &&）都以copy轉換

>member function pointer 及 member pointer 定義的函式須以`o:member(...)`或`o.member(o, ...)`形式使用

**LUAPP_LIBRARY(class, base, library_name, table)**

>*class* = 型態

>*base* = 繼承的型態（無繼承或不想在lua內繼承可定義為lua::undefined_t）

>*name* = lua library名稱字串

>*table* = LUAPP_TABLE之定義

**LUAPP_TABLE(...)**

>定義metatable，於LUA_LIBRARY參數table使用

**LUAPP_NEW**

>定義new函式，於LUAPP_TABLE內使用

>使用此巨集類別須提供default constructor

**LUAPP_FUNCTION(member)**

>定義lua function，可以是member function或一般function

**LUAPP_SYNTHESIZE(member)**

>定義lua function，支援member pointer轉換成類似property功能的function

>將會產生兩組lua function

>取值定義為function member(o)，回傳native值

>賦值定義為function member(o, value)，無回傳值

## 以下待編輯未完...