
#pragma once
#include <type_traits>
#include <utility>

namespace lua
{
    /*
        name_value
            name_value is std::pair<const char*, type>
    */
    template<typename Ty>
    using name_value = std::pair<const char*, Ty>;

    template<typename Ty>
    constexpr
    name_value<Ty> make_nv(const char* name, Ty && value)
    {
        return{ name, value };
    }

    /*
        undefined_t
            define undefined type for traits
    */
    struct undefined_t {};

    template<typename Ty>
    using is_undefined_t = typename std::is_base_of<undefined_t, Ty>::type;

    /*
        arg
            arg::iterator to trait variadic template argiments
    */
    namespace args
    {
        template<typename... Args>
        struct iterator;

        template<typename Ty, typename... Args>
        struct iterator<Ty, Args...>
        {
            using type = Ty;
            using forward_t = iterator<Args...>;
        };

        template<>
        struct iterator<> : undefined_t
        {
        };
    }

    /*
    
        optional
            optional to make value nullable
    
    */
    using nullopt_t = undefined_t;
    constexpr nullopt_t nullopt = nullopt_t();

    template<typename Ty>
    struct optional
    {
        optional()
            : owned_(false)
        {
        }

        optional(nullopt_t)
            : owned_(false)
        {
        }

        optional(optional const & other)
            : owned_(other.owned_)
        {
            if (owned_)
                ::new(data_) Ty(*other.get());
        }

        optional(optional && other)
            : owned_(other.owned_)
        {
            if (owned_)
                ::new(data_) Ty(other.release());
        }

        optional(Ty const & value)
            : owned_(true)
        {
            ::new(data_) Ty(value);
        }

        optional(Ty && value)
            : owned_(true)
        {
            ::new(data_) Ty(std::forward<Ty>(value));
        }

        template<class... Args>
        explicit optional(Args &&... args)
            : owned_(true)
        {
            ::new(data_) Ty(std::forward<Args>(args)...);
        }

        ~optional()
        {
            reset();
        }

        constexpr explicit operator bool() const
        {
            return owned_;
        }

        constexpr Ty const * operator->() const
        {
            return owned_ ? get() : nullptr;
        }

        inline Ty* operator->()
        {
            return owned_ ? get() : nullptr;
        }

        constexpr Ty const & operator*() const
        {
            return *get();
        }

        inline Ty & operator*()
        {
            return *get();
        }

        template<typename... Args>
        inline optional & operator =(Args &&... args)
        {
            reset(std::forward<Args>(args)...);
            return *this;
        }

    protected:
        void reset()
        {
            if (owned_)
            {
                get()->~Ty();
                owned_ = false;
            }
        }

        template<typename... Args>
        void reset(Args &&... args)
        {
            reset();
            owned_ = true;
            ::new(data_) Ty(std::forward<Args>(args)...);
        }

        void reset(optional && other)
        {
            reset();
            owned_ = other.owned_;
            if (owned_)
                ::new(data_) Ty(other.release());
        }

        void reset(nullopt_t)
        {
            reset();
        }

        Ty release()
        {
            Ty retval = std::move(*get());
            reset();
            return std::move(retval);
        }
        Ty * get()
        {
            return reinterpret_cast<Ty*>(data_);
        }
        Ty const * get() const
        {
            return reinterpret_cast<Ty const *>(data_);
        }

        std::aligned_storage_t<sizeof(Ty)> data_[1];
        bool owned_;
    };
}