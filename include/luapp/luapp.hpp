
#pragma once
#include <luapp/utility.h>

// luajit library
extern "C"
{
#include <lua.h>
#include <lauxlib.h>
}

// std library
#include <utility>
#include <cstddef>
#include <string>
#include <functional>
#include <memory>

namespace lua
{
    /*

        lua definitions
            useful type definition for
            lua_State, std::string, std::function

    */
    using handle = lua_State*;
    using string = std::string;

    enum class classify
    {
        unknown,
        general,
        function,
        shared_ptr,
        optional,
    };

    template<typename Ty>
    struct classify_of : std::integral_constant<classify,
        std::is_class<Ty>::value ? classify::general : classify::unknown
    >
    {
    };
    template<typename Fn>
    struct classify_of<std::function<Fn>> : std::integral_constant<classify, classify::function>
    {
    };
    template<typename Ty>
    struct classify_of<std::shared_ptr<Ty>> : std::integral_constant<classify, classify::shared_ptr>
    {
    };
    template<typename Ty>
    struct classify_of<lua::optional<Ty>> : std::integral_constant<classify, classify::optional>
    {
    };
    template<typename Ty, classify cval>
    struct is_classify_of : std::integral_constant<bool, classify_of<Ty>::value == cval>
    {
    };
    template<typename Ty>
    struct is_general_class : is_classify_of<Ty, classify::general>::type
    {
    };

    /*
    
        lua error handler
    
    */
    using error_function = std::function<void(int, std::string const &)>;
    extern void set_error_handler(error_function && cb);

    /*

        ref_base
            make reference to lua value at top of stack with pop it.
            use push() to push the referenced value into stack and return current state of lua

    */
    class ref_base
    {
    public:
        ref_base() = default;

        ref_base(lua::handle lua)
        {
            reset(lua);
        }

        ref_base(std::nullptr_t)
            : ref_base()
        {
        }

        ref_base(ref_base const & other)
        {
            reset(other);
        }

        ref_base(ref_base && other)
        {
            reset(std::move(other));
        }

        ~ref_base()
        {
            reset(nullptr);
        }

        inline
        lua::handle push() const
        {
            if (nullptr != L)
            {
                lua_rawgeti(L, LUA_REGISTRYINDEX, registry);
            }
            return L;
        }

        inline
        ref_base & operator =(ref_base const & other)
        {
            reset(other);
            return *this;
        }

        inline
        ref_base & operator =(ref_base && other)
        {
            reset(std::move(other));
            return *this;
        }

        inline
        ref_base & operator =(std::nullptr_t)
        {
            reset(nullptr);
            return *this;
        }

        inline
        void reset(std::nullptr_t)
        {
            if (nullptr != L)
            {
                luaL_unref(L, LUA_REGISTRYINDEX, registry);
                L = nullptr;
            }
        }

        inline
        void reset(ref_base && other)
        {
            reset(nullptr);

            registry = other.registry;
            L = other.L;
            other.L = nullptr;
        }

        inline
        void reset(ref_base const & other)
        {
            reset(nullptr);

            if (!other.is_nil())
            {
                L = other.push();
                registry = luaL_ref(L, LUA_REGISTRYINDEX);
            }
        }

        inline
        void reset(handle lua)
        {
            reset(nullptr);

            if (!lua_isnoneornil(lua, -1))
            {
                L = lua;
                registry = luaL_ref(L, LUA_REGISTRYINDEX);
            }
        }

        inline
        ref_base at(std::string const & name) const
        {
            push();
            lua_getfield(L, -1, name.c_str());
            lua_remove(L, -2);
            return ref_base(L);
        }

        inline
        ref_base rawget(std::string const & name) const
        {
            push();
            lua_pushlstring(L, name.c_str(), name.length());
            lua_rawget(L, -2);
            lua_remove(L, -2);
            return ref_base(L);
        }

        inline
        bool is_nil() const
        {
            return nullptr == L;
        }

        inline
        operator bool() const
        {
            return is_nil();
        }

    private:
        lua::handle L = nullptr;
        int registry;
    };

    /*

        ref
            make reference to lua value at top of stack with pop it.

    */
    template<typename Ty>
    class ref
    {
    public:
        ref() = default;
        ~ref() = default;

        ref(ref_base && r)
            : ref_(r)
        {
        }

        ref(ref_base const & r)
            : ref_(r)
        {
        }

        template<typename Tz>
        ref(ref<Tz> && other)
            : ref_(std::move(other.ref_))
        {
        }

        template<typename Tz>
        ref(ref<Tz> const & other)
            : ref_(other.ref_)
        {
        }

        ref & operator =(ref_base && r)
        {
            ref_.reset(std::move(r));
            return *this;
        }
        ref & operator =(ref_base const & r)
        {
            ref_.reset(r);
            return *this;
        }
        template<typename Tz>
        ref & operator =(ref<Tz> const & other)
        {
            ref_.reset(other.ref_);
            return *this;
        }
        template<typename Tz>
        ref & operator =(ref<Tz> && other)
        {
            ref_.reset(std::move(other.ref_));
            return *this;
        }

        inline
        Ty get() const
        {
            lua::handle L = ref_.push();
            Ty retval = detail::to_value<Ty>(L, -1);
            lua_pop(L, 1);
            return std::move(retval);
        }

        constexpr
        operator Ty () const
        {
            return get();
        }

        constexpr
            lua::handle push() const
        {
            return ref_.push();
        }

        constexpr
        bool is_nil() const
        {
            return ref_.is_nil();
        }

        template<typename _Ty = Ty, typename... Args>
        inline
        std::enable_if_t<
            std::is_void<_Ty>::value
        , _Ty>
            operator ()(Args &&... args) const
        {
            lua::handle L = ref_.push();
            push_arg(L, std::forward<Args>(args)...);
            lua::call(L, sizeof...(Args), 0);
        }

        template<typename _Ty = Ty, typename... Args>
        inline
        std::enable_if_t<
            !std::is_void<_Ty>::value
        , ref<_Ty>>
            operator ()(Args &&... args) const
        {
            lua::handle L = ref_.push();
            push_arg(L, std::forward<Args>(args)...);
            if (lua::call(L, sizeof...(Args), 1))
            {
                return ref<_Ty>(ref_base(L));
            }
            return ref<_Ty>();
        }

    private:
        constexpr
        void push_arg(lua::handle) const
        {
        }
        template<typename _Ty, typename... _Args>
        constexpr
        void push_arg(lua::handle L, _Ty && val, _Args &&... args) const
        {
            detail::push(L, val);
            push_arg(L, std::forward<_Args>(args)...);
        }

        ref_base ref_;
    };

    /*
    
        top_guard
            utility for guard top of stack
    
    */
    class top_guard
    {
    public:
        top_guard(lua::ref_base const & ref)
        {
            if (L = ref.push())
                top = lua_gettop(L) - 1;
        }

        ~top_guard()
        {
            release();
        }

        inline void release()
        {
            if (L)
            {
                lua_settop(L, top);
                L = nullptr;
            }
        }

        inline operator bool() const
        {
            return nullptr != L;
        }

        int top;
        lua::handle L = nullptr;
    };

    /*
    
        api warpper
    
    */
    extern bool call(lua::handle L, int narg = 0, int nresult = 0);

    namespace detail
    {
        /*

            value
                solution to provide lua value access
                provided:
                    push() to push c value into lua stack
                    get() to get c value from lua stack

        */
        template<typename Ty, typename = void>
        struct value;

        /*

            function_pointer
                solution to provide lua c closure wrapper to custom c functions
                function_pointer<signature>::invoke is lua_CFunction which automatic wrap type of function

        */
        template<typename Fn>
        struct function_pointer;

        /*

            function_object
                solution to provide lua c closure wrapper to functors
                function_object<signature, functor_type>::invoke
                is lua_CFunction which automatic wrap functor

        */
        template<typename Fn, typename Ty>
        struct function_object;

        /*
            finalizer
                gc helper
        */
        template<typename Ty>
        constexpr
        int finalizer(handle L)
        {
            static_cast<Ty*>(lua_touserdata(L, 1))->~Ty();
            return 0;
        }
        template<typename Ty>
        constexpr
        std::enable_if_t<
            std::is_trivially_destructible<Ty>::value
        >
            set_finalizer(handle L)
        {
        }
        template<typename Ty>
        constexpr
        std::enable_if_t<
            !std::is_trivially_destructible<Ty>::value
        >
            set_finalizer(handle L)
        {
            lua_pushliteral(L, "__gc");
            lua_pushcclosure(L, finalizer<Ty>, 0);
            lua_rawset(L, -3);
        }
        template<typename Ty>
        constexpr
        std::enable_if_t<
            std::is_trivially_destructible<Ty>::value
        >
            set_metatable_with_finlizer(lua::handle L)
        {
        }
        template<typename Ty>
        constexpr
        std::enable_if_t<
            !std::is_trivially_destructible<Ty>::value
        >
            set_metatable_with_finlizer(lua::handle L)
        {
            lua_createtable(L, 0, 1);
            set_finalizer<Ty>(L);
            lua_setmetatable(L, -2);
        }

        /*
            new_operator
                support lua library to generate a type in script
        */
        template<typename Ty>
        constexpr
        int new_operator(handle L);

        /*
            get_function
                support lua library to get member value in script
        */
        template<typename Ty>
        struct get_function;

        /*
            set_function
                support lua library to set member value in script
        */
        template<typename Ty>
        struct set_function;

        /*

            push
                wrapper to value<Type>::push() with sequence

        */
        constexpr
        int push(lua::handle) { return 0; }

        template<typename Ty>
        constexpr
        int push(lua::handle L, Ty && val)
        {
            return value<std::decay_t<Ty>>::push(L, val);
        }

        template<typename Ret, typename... Args>
        constexpr
        int push(lua::handle L, Ret(*f)(Args...))
        {
            lua_pushlightuserdata(L, f);
            lua_pushcclosure(L, function_pointer<Ret(Args...)>::invoke, 1);
            return 1;
        }

        template<typename Fn>
        constexpr
        int push(lua::handle L, std::function<Fn> && fn)
        {
            return function_value<Fn>::push(L, fn);
        }

        inline
        int push(lua::handle L, lua_CFunction f)
        {
            lua_pushcclosure(L, f, 0);
            return 1;
        }

        // specified for literal string
        template<unsigned N>
        constexpr
        int push(lua::handle L, char const (&s)[N])
        {
            lua_pushlstring(L, s, N - 1);
            return 1;
        }

        template<typename Ty, typename... Args>
        constexpr
        int push(lua::handle L, Ty && val, Args &&... args)
        {
            return 
                push(L, std::forward<Ty>(val)) +
                push(L, std::forward<Args>(args)...);
        }

        /*

            push_func
                wrapper to function_object<Function, Type>::push()

        */
        template<typename Fn, typename Ty>
        constexpr int push_func(lua::handle L, Ty && obj)
        {
            return detail::function_object<Fn, Ty>::push(L, std::forward<Ty>(obj));
        }

        /*
            
            push
                member function pointer
            
        */
        template<typename Ret, typename Arg0, typename... Args>
        constexpr
        int push(lua::handle L, Ret(Arg0::*f)(Args...))
        {
            return push_func<Ret(Arg0*, Args...)>(L, std::mem_fn(f));
        }
        template<typename Ret, typename Arg0, typename... Args>
        constexpr
        int push(lua::handle L, Ret(Arg0::*f)(Args...) const)
        {
            return push_func<Ret(Arg0*, Args...)>(L, std::mem_fn(f));
        }
        template<typename Ty>
        constexpr
        int push(lua::handle L, get_function<Ty> && f)
        {
            return push_func<typename get_function<Ty>::function>(L,
                std::forward<get_function<Ty>>(f));
        }
        template<typename Ty>
        constexpr
        int push(lua::handle L, set_function<Ty> && f)
        {
            return push_func<typename set_function<Ty>::function>(L,
                std::forward<set_function<Ty>>(f));
        }

        /*

            to_value
                wrapper of value<Type>::to_value()

        */
        template<typename Ty>
        constexpr
        std::decay_t<Ty> to_value(lua::handle L, int index)
        {
            return value<std::decay_t<Ty>>::get(L, index);
        }
        // special case for string
        template<>
        inline
        const char* to_value<const char*>(lua::handle L, int index)
        {
            return lua_tolstring(L, index, nullptr);
        }

        /*

            value partial implementations
                supported type:
                    integer types ( int, short, char ... )
                    floating types ( float, double )
                    boolean ( bool )
                    std::string
                    char const *
                    std::function ( return and arguments requires lua library defined type )
                    ref<type> ( lua library defined type )
                    pointer of types ( lua library defined type )
                    types ( lua library defined type )

        */
        /* integer types */
        template<typename Ty>
        struct value<Ty, std::enable_if_t<std::is_integral<Ty>::value>>
        {
            enum { count = 1 };

            static constexpr
            Ty get(lua::handle L, int index)
            {
                return static_cast<Ty>(lua_tointeger(L, index));
            }

            static constexpr
            int push(lua::handle L, Ty val)
            {
                lua_pushinteger(L, static_cast<lua_Integer>(val));
                return count;
            }
        };

        /* floating types */
        template<typename Ty>
        struct value<Ty, std::enable_if_t<std::is_floating_point<Ty>::value>>
        {
            enum { count = 1 };

            static constexpr
            Ty get(lua::handle L, int index)
            {
                return static_cast<Ty>(lua_tonumber(L, index));
            }

            static constexpr
            int push(lua::handle L, Ty val)
            {
                lua_pushnumber(L, static_cast<lua_Number>(val));
                return count;
            }
        };

        /* std::string */
        template<>
        struct value<lua::string, void>
        {
            enum { count = 1 };

            static inline
            lua::string get(lua::handle L, int index)
            {
                size_t len;
                auto str = lua_tolstring(L, index, &len);
                if (nullptr != str)
                {
                    return lua::string(str, len);
                }
                return lua::string();
            }

            static inline
            int push(lua::handle L, lua::string const & val)
            {
                lua_pushlstring(L, val.c_str(), val.length());
                return count;
            }
        };
        template<typename Ty>
        struct value<Ty*, std::enable_if_t<std::is_same<std::decay_t<Ty>, char>::value>>
        {
            enum { count = 1 };

            static constexpr
            Ty* get(lua::handle L, int index)
            {
                return lua_tostring(L, index);
            }

            static constexpr
            int push(lua::handle L, Ty* val)
            {
                lua_pushstring(L, val);
                return count;
            }
        };
            
        /* boolean */
        template<>
        struct value<bool, void>
        {
            enum { count = 1 };

            static inline
            bool get(lua::handle L, int index)
            {
                return (0 != lua_toboolean(L, index)) ? true : false;
            }

            static inline
            int push(lua::handle L, bool val)
            {
                lua_pushboolean(L, val ? 1 : 0);
                return count;
            }
        };

        /* enum */
        template<typename Ty>
        struct value<Ty, std::enable_if_t<std::is_enum<Ty>::value>>
        {
            enum { count = 1 };

            using value_type = std::underlying_type_t<Ty>;

            static constexpr
            Ty get(lua::handle L, int index)
            {
                return (Ty)value<value_type>::get(L, index);
            }

            static constexpr
            int push(lua::handle L, Ty val)
            {
                return value<value_type>::push(L, (value_type)val);
            }
        };

        /* std::function */
        template<typename Return, typename... Args>
        struct value<std::function<Return(Args...)>, void>
        {
            enum { count = 1 };

            static constexpr
            std::function<Return(Args...)> get(lua::handle L, int index)
            {
                lua_pushvalue(L, index);
                return [luaRef = ref_base(L)]
                    (Args &&... args) -> Return
                {
                    lua::handle L = luaRef.push();
                    detail::push(L, std::forward<Args>(args)...);
                    if (lua::call(L, sizeof...(Args), value<Return>::count, 0))
                    {
                        Return retval = to_value<Return>(L, -1);
                        lua_pop(L, value<Return>::count);
                        return std::move(retval);
                    }
                    return Return();
                };
            }

            static constexpr
            int push(lua::handle L, std::function<Return(Args...)> && fn)
            {
                using Fn = Return(Args...);
                using Ty = std::function<Fn>;

                ::new(lua_newuserdata(L, sizeof(Ty))) Ty(obj);
                set_metatable_with_finlizer<Ty>(L);
                lua_pushcclosure(L, detail::function_object<Fn, Ty>::invoke, 1);
                return count;
            }
        };
        template<typename... Args>
        struct value<std::function<void(Args...)>, void>
        {
            enum { count = 1 };

            static inline
            std::function<void(Args...)> get(lua::handle L, int index)
            {
                if (lua_isfunction(L, index))
                {
                    lua_pushvalue(L, index);
                    return[luaRef = ref_base(L)]
                        (Args &&... args) -> void
                    {
                        lua::handle L = luaRef.push();
                        detail::push(L, std::forward<Args>(args)...);
                        lua::call(L, sizeof...(Args), 0);
                    };
                }
                else
                    return nullptr;
            }

            static inline
            int push(lua::handle L, std::function<void(Args...)> && fn)
            {
                if (fn)
                {
                    using Fn = Return(Args...);
                    using Ty = std::function<Fn>;

                    ::new(lua_newuserdata(L, sizeof(Ty))) Ty(obj);
                    set_metatable_with_finlizer<Ty>(L);
                    lua_pushcclosure(L, detail::function_object<Fn, Ty>::invoke, 1);
                }
                else
                {
                    lua_pushnil(L);
                }
                return count;
            }
        };

        /* ref_base */
        template<>
        struct value<lua::ref_base, void>
        {
            enum { count = 1 };

            static constexpr
            lua::ref_base get(lua::handle L, int index)
            {
                lua_pushvalue(L, index);
                return lua::ref_base(L);
            }

            static inline
            int push(lua::handle L, lua::ref_base const & val)
            {
                if (val.is_nil())
                    lua_pushnil(L);
                else
                    val.push();
                return 1;
            }
        };

        /* ref<type> */
        template<typename Ty>
        struct value<ref<Ty>, void>
        {
            enum { count = 1 };

            static constexpr
            ref<Ty> get(lua::handle L, int index)
            {
                lua_pushvalue(L, index);
                return ref<Ty>(ref_base(L));
            }

            static constexpr
            int push(lua::handle L, ref<Ty> const & val)
            {
                return (L == val.push()) ? count : 0;
            }
        };

        /*

            function_pointer implementations

        */
        template<typename Return, typename... Args>
        struct function_pointer<Return(Args...)>
        {
            using closure_t = Return(*)(Args...);

            template<typename _Return, typename... _Args>
            static constexpr
            std::enable_if_t<
                std::is_void<_Return>::value
            , int>
                invoke(lua::handle L, _Args &&... args)
            {
                static_cast<closure_t>(
                    lua_topointer(L, lua_upvalueindex(1)))
                    (std::forward<_Args>(args)...);
                return 0;
            }

            template<typename _Return, typename... _Args>
            static constexpr
            std::enable_if_t<
                !std::is_void<_Return>::value
            , int>
                invoke(lua::handle L, _Args &&... args)
            {
                return detail::push(L,
                    static_cast<closure_t>(
                        lua_topointer(L, lua_upvalueindex(1)))
                    (
                        std::forward<_Args>(args)...
                    )
                );
            }

            template<typename Iter, typename... _Args>
            static constexpr
            std::enable_if_t<
                is_undefined_t<Iter>::value
            , int>
                unpack(lua::handle L, int index, _Args &&... args)
            {
                return invoke<Return>(L, std::forward<_Args>(args)...);
            }

            template<typename Iter, typename... _Args>
            static constexpr
            std::enable_if_t<
                !is_undefined_t<Iter>::value
            , int>
                unpack(lua::handle L, int index, _Args &&... args)
            {
                using arg_type = std::decay_t<typename Iter::type>;
                using forward_type = typename Iter::forward_t;

                return unpack<forward_type>(
                    L, index + 1, std::forward<_Args>(args)..., to_value<arg_type>(L, index));
            }

            static constexpr
            int invoke(lua::handle L)
            {
                return unpack<args::iterator<Args...>>(L, 1);
            }
        };

        /*

            function_object implementations

        */
        template<typename Ty, typename Return, typename... Args>
        struct function_object<Return(Args...), Ty>
        {
            template<typename _Return, typename... _Args>
            static constexpr
            std::enable_if_t<
                std::is_void<_Return>::value
            , int>
                invoke(lua::handle L, _Args &&... args)
            {
                (*static_cast<Ty*>(
                    lua_touserdata(L, lua_upvalueindex(1))))
                    (std::forward<_Args>(args)...);
                return 0;
            }

            template<typename _Return, typename... _Args>
            static constexpr
            std::enable_if_t<
                !std::is_void<_Return>::value
            , int>
                invoke(lua::handle L, _Args &&... args)
            {
                using ret_type = std::decay_t<_Return>;

                return value<ret_type>::push(L,
                    (*static_cast<Ty*>(
                        lua_touserdata(L, lua_upvalueindex(1))))
                    (
                        std::forward<_Args>(args)...
                    )
                );
            }

            template<typename Iter, typename... _Args>
            static constexpr
            std::enable_if_t<
                is_undefined_t<Iter>::value
            , int>
                unpack(lua::handle L, int index, _Args &&... args)
            {
                return invoke<Return>(L, std::forward<_Args>(args)...);
            }

            template<typename Iter, typename... _Args>
            static constexpr
            std::enable_if_t<
                !is_undefined_t<Iter>::value
            , int>
                unpack(lua::handle L, int index, _Args &&... args)
            {
                using arg_type = typename Iter::type;
                using forward_type = typename Iter::forward_t;

                return unpack<forward_type>(
                    L, index + 1, std::forward<_Args>(args)..., to_value<arg_type>(L, index));
            }

            static constexpr
            int invoke(lua::handle L)
            {
                return unpack<args::iterator<Args...>>(L, 1);
            }

            static constexpr
            int push(lua::handle L, Ty && obj)
            {
                ::new(lua_newuserdata(L, sizeof(Ty))) Ty(obj);
                set_metatable_with_finlizer<Ty>(L);
                lua_pushcclosure(L, invoke, 1);
                return 1;
            }
        };
    } // detail

    /*

        open_lib
            open lua library with type has defined by LUAPP_LIBRARY

    */
    struct lib_desc
    {
        int table;
    };
    template<typename Ty>
    lib_desc open_lib();

    /*

        lua library support utilities

    */
    namespace detail
    {
        template<typename Ty>
        constexpr
        void rawset(lua::handle L, name_value<Ty> && nv)
        {
            lua_pushstring(L, nv.first);
            push(L, std::forward<Ty>(nv.second));
            lua_rawset(L, -3);
        }

        template<typename Ty, typename... Args>
        constexpr
        void rawset(lua::handle L, name_value<Ty> && nv, name_value<Args> &&... args)
        {
            rawset(L, std::forward<name_value<Ty>>(nv));
            rawset(L, std::forward<name_value<Args>>(args)...);
        }

        template<typename... Args>
        constexpr
        void create_table(lua::handle L, name_value<Args> &&... args)
        {
            lua_createtable(L, 0, sizeof...(Args));
            rawset(L, std::forward<name_value<Args>>(args)...);
        }

        template<typename Ty>
        struct metatable
        {
            static constexpr
            void create(lua::handle L)
            {
                lua_createtable(L, 0, 1);
                lua_pushliteral(L, "__index");
                lua_rawgeti(L, LUA_REGISTRYINDEX, open_lib<Ty>().table);
                lua_rawset(L, -3);
            }
        };
        template<>
        struct metatable<lua::undefined_t>
        {
            static inline
            void create(lua::handle L)
            {
                lua_pushnil(L);
            }
        };

        template<typename Ty>
        constexpr
        void create_metatable(lua::handle L)
        {
            return metatable<Ty>::create(L);
        }

        template<typename TBase>
        inline
        lib_desc finalize_lib(lua::handle L, const char* name)
        {
            lib_desc desc;
            create_metatable<TBase>(L);
            lua_setmetatable(L, -2);
            lua_pushvalue(L, -1);
            lua_setglobal(L, name);
            desc.table = luaL_ref(L, LUA_REGISTRYINDEX);
            return desc;
        }

        /*

            get_function implementation

        */
        template<typename Arg0, typename Ty>
        struct get_function<Ty(Arg0::*)>
        {
            using pointer = Ty(Arg0::*);
            using function = Ty(Arg0*);

            get_function(pointer p)
                : p_(p)
            {
            }

            constexpr
            Ty operator()(Arg0* p) const
            {
                return (p->*p_);
            }

            pointer p_;
        };
        template<typename Ty>
        constexpr
        get_function<Ty> make_get(Ty p)
        {
            return get_function<Ty>(p);
        }

        /*

            set_function implementation

        */
        template<typename Arg0, typename Ty>
        struct set_function<Ty(Arg0::*)>
        {
            using pointer = Ty(Arg0::*);
            using function = void(Arg0*, Ty);

            set_function(pointer p)
                : p_(p)
            {
            }

            constexpr
            void operator()(Arg0* p, Ty val) const
            {
                (p->*p_) = val;
            }

            pointer p_;
        };
        template<typename Ty>
        constexpr
        set_function<Ty> make_set(Ty p)
        {
            return set_function<Ty>(p);
        }

    } // detail

    /*
        
        lua library define macros

    */
#define LUAPP_LIBRARY(klass, base, name, table)         \
template<> lua::lib_desc                                \
lua::open_lib<klass>() {                                \
using this_type = klass;                                \
static auto desc = []() {                               \
auto L = lua::state::getInstance()->native_handle();    \
table                                                   \
return lua::detail::finalize_lib<base>(L, name);        \
}();                                                    \
return desc;                                            \
}                                                        
#define LUAPP_TABLE(...)                                \
lua::detail::create_table(L, __VA_ARGS__);
#define LUAPP_FUNCTION(member)                          \
make_nv(#member, &this_type::member)
#define LUAPP_FUNCTION_WITH_NAME(member, name)          \
make_nv(name, &this_type::member)
#define LUAPP_SYNTHESIZE(member)                        \
make_nv(#member,                                        \
lua::detail::make_get(&this_type::member)),             \
make_nv("set_"#member,                                  \
lua::detail::make_set(&this_type::member))
#define LUAPP_NEW                                       \
make_nv("new", &lua::detail::new_operator<this_type>)

    namespace detail
    {
        /*
        
            userdata 
                managed/unmanaged/shared userdata structure

        */
        class userdata
        {
        public:
            virtual ~userdata() = default;
            virtual void* get() = 0;
        };
        template<typename Ty>
        class managed : public userdata
        {
        public:
            managed() = default;

            managed(Ty const & val)
                : value(val)
            {
            }
            managed(Ty && val)
                : value(std::move(val))
            {
            }

            virtual ~managed() = default;
            
            virtual void* get()
            {
                return &value;
            }

            Ty value;
        };
        template<typename Ty>
        class unmanaged : public userdata
        {
        public:
            unmanaged(Ty * val)
                : value(val)
            {
            }

            virtual ~unmanaged() = default;

            virtual void* get()
            {
                return value;
            }

            Ty * value;
        };
        template<typename Ty>
        class shared : public userdata
        {
        public:
            shared(std::shared_ptr<Ty> const & val)
                : value(val)
            {
            }

            virtual ~shared() = default;

            virtual void* get()
            {
                return value.get();
            }

            std::shared_ptr<Ty> value;
        };

        template<typename Ty>
        constexpr
        Ty* touserdata(lua::handle L, int index)
        {
            return static_cast<Ty*>(static_cast<userdata*>(lua_touserdata(L, index))->get());
        }

        /*
            value ( library type )
                to get/push value of type will be a copy
        */
        template<typename Ty>
        struct value<Ty, std::enable_if_t<is_general_class<Ty>::value>>
        {
            enum { count = 1 };

            static constexpr
            Ty get(lua::handle L, int index)
            {
                return Ty(*touserdata<Ty>(L, index));
            }

            static constexpr
            int push(lua::handle L, Ty && val)
            {
                ::new(lua_newuserdata(L, sizeof(managed<Ty>))) managed<Ty>(val);
                create_metatable<Ty>(L);
                set_finalizer<managed<Ty>>(L);
                lua_setmetatable(L, -2);
                return count;
            }

            static constexpr
            int push(lua::handle L, Ty const & val)
            {
                return push(L, Ty(val));
            }
        };

        /*
            value ( pointer of library type )
                to get/push pointer of type will be lightuserdata in lua,
                if you want to get/push a script managed pointer ( userdata ) with keep reference, 
                please use ref<type> as good and safe
        */
        template<typename Ty>
        struct value<Ty*, std::enable_if_t<is_general_class<std::decay_t<Ty>>::value>>
        {
            enum { count = 1 };

            static constexpr
            Ty* get(lua::handle L, int index)
            {
                return touserdata<Ty>(L, index);
            }

            static constexpr
            int push(lua::handle L, Ty* val)
            {
                using Type = std::decay_t<Ty>;
                ::new(lua_newuserdata(L, sizeof(unmanaged<Type>))) unmanaged<Type>(val);
                create_metatable<Type>(L);
                set_finalizer<unmanaged<Type>>(L);
                lua_setmetatable(L, -2);
                return count;
            }
        };

        /*

            value ( shared_ptr of library type )

        */
        template<typename Ty>
        struct value<std::shared_ptr<Ty>, std::enable_if_t<is_general_class<Ty>::value>>
        {
            enum { count = 1 };

            static inline
            std::shared_ptr<Ty> get(lua::handle L, int index)
            {
                if (lua_isuserdata(L, index))
                    return touserdata<Ty>(L, index)->shared_from_this();
                else
                    return nullptr;
            }

            static inline
            int push(lua::handle L, std::shared_ptr<Ty> const & val)
            {
                if (val)
                {
                    ::new(lua_newuserdata(L, sizeof(shared<Ty>))) shared<Ty>(val);
                    create_metatable<Ty>(L);
                    set_finalizer<shared<Ty>>(L);
                    lua_setmetatable(L, -2);
                }
                else
                {
                    lua_pushnil(L);
                }
                return count;
            }
        };

        /*
        
            value ( optional for supported type )

        */
        template<typename Ty>
        struct value<lua::optional<Ty>, void>
        {
            enum { count = 1 };

            static inline
            lua::optional<Ty> get(lua::handle L, int index)
            {
                if (lua_isnoneornil(L, index))
                {
                    return lua::nullopt;
                }
                else
                {
                    return detail::value<Ty>::get(L, index);
                }
            }

            static inline
            int push(lua::handle L, lua::optional<Ty> const & opt)
            {
                if (opt)
                {
                    return detail::value<Ty>::push(L, *opt);
                }
                else
                {
                    lua_pushnil(L);
                    return count;
                }
            }

            static inline
            int push(lua::handle L, lua::optional<Ty> && opt)
            {
                if (opt)
                {
                    return detail::value<Ty>::push(L, std::move(*opt));
                }
                else
                {
                    lua_pushnil(L);
                    return count;
                }
            }
        };

        /*

            new_operator implementation

        */
        template<typename Ty>
        constexpr
        int new_operator(lua::handle L)
        {
            ::new(lua_newuserdata(L, sizeof(managed<Ty>))) managed<Ty>();
            create_metatable<Ty>(L);
            set_finalizer<managed<Ty>>(L);
            lua_setmetatable(L, -2);
            return 1;
        }
    }

    /*
            
        state initial function

    */
    enum class initial
    {
        simple,
        default_libs,
    };

    /*

        state 
            class to handle lua operations
                features: non-copyable, moveable, shared
                native_handle() to get lua_State object

    */
    class state
    {
    public:
        state(initial what = initial::simple);
        ~state();

        state(state && other);
        state & operator =(state && other);

        inline
        lua::handle native_handle() const
        {
            return L;
        }

        template<typename Ty>
        constexpr
        int push(Ty && val) const
        {
            return detail::push(L, std::forward<Ty>(val));
        }

        lua::ref_base get_global(std::string const & name) const;
        lua::ref_base call(std::string const & text) const;
        lua::ref_base load(std::string const & text) const;
        lua::ref_base load(const char* buff, size_t sz, std::string const & name) const;

        static state * getInstance();
        static void destroyInstance();

    private:
        state(state const &) = delete;
        state & operator =(state const &) = delete;
        lua::handle L = nullptr;
    };
}