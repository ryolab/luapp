
#include <luapp/luapp.hpp>

#include <stdio.h>

class foo
{
public:
    ~foo()
    {
        printf("foo drop!\n");
    }

    int hello(const char* say, const char* what)
    {
        printf("%s %s\n", say, what);
        return value;
    }

    int value = 567;
};

LUAPP_LIBRARY(foo, lua::undefined_t, "foo",
    LUAPP_TABLE(
        LUAPP_NEW,
        LUAPP_FUNCTION(hello),
        LUAPP_SYNTHESIZE(value)
    );
);

int main()
{
    lua::open_lib<foo>();

    auto & S = lua::state::shared();
    auto L = S.native_handle();

    luaL_dofile(L, "scripts/foo.lua");

    lua::ref<std::string> fn = S.get_global("hey");
    printf("%s\n", fn("Hi Lua!").get().c_str());

    return 0;
}